<?php

use Phinx\Migration\AbstractMigration;

class Speaker extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('speaker', ['id' => false, 'primary_key' => 'speaker_uuid'])
            ->addColumn('speaker_uuid', 'binary', ['limit' => 16])
            ->addColumn('speaker_id', 'text')
            ->addColumn('name', 'text')
            ->addColumn('description', 'text')
            ->addColumn('slug', 'text')
            ->addColumn('twitter', 'text', ['null' => true])
            ->addColumn('facebook', 'text', ['null' => true])
            ->addColumn('main_img', 'text', ['null' => true])
            ->addColumn('company', 'text', ['null' => true])
            ->addColumn('is_active', 'boolean', ['default' => false])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}
