<?php

use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;

$configManager = new ConfigAggregator([

    // App packages
    \Category\ConfigProvider::class,
    \Article\ConfigProvider::class,
    \Menu\ConfigProvider::class,
    \Admin\ConfigProvider::class,
    \Newsletter\ConfigProvider::class,
    \Speaker\ConfigProvider::class,
    \Page\ConfigProvider::class,
    \Talk\ConfigProvider::class,
    \Meetup\ConfigProvider::class,
    \Web\ConfigProvider::class,

    new PhpFileProvider('config/autoload/{{,*.}global,{,*.}local}.php'),
]);

return new ArrayObject($configManager->getMergedConfig());
