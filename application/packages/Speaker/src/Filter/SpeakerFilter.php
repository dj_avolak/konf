<?php

namespace Speaker\Filter;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SpeakerFilter implements InputFilterAwareInterface
{
    protected $inputFilter;

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();

        $inputFilter->add(
            [
            'name'       => 'name',
            'required'   => true,
            'filters'    => [['name' => 'StringTrim']],
            'validators' => [
                ['name' => 'NotEmpty'],
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
            ],
            ]
        );

        $inputFilter->add(
            [
            'name'       => 'slug',
            'required'   => true,
            'filters'    => [['name' => 'StringTrim']],
            'validators' => [
                ['name' => 'NotEmpty'],
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
            ],
            ]
        );

        $inputFilter->add(
            [
                'name'       => 'company',
                'required'   => false,
                'filters'    => [['name' => 'StringTrim']],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
                ],
            ]
        );

        $inputFilter->add(
            [
                'name'       => 'twitter',
                'required'   => false,
                'filters'    => [['name' => 'StringTrim']],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
                ],
            ]
        );

        $inputFilter->add(
            [
                'name'       => 'facebook',
                'required'   => false,
                'filters'    => [['name' => 'StringTrim']],
                'validators' => [
                    ['name' => 'NotEmpty'],
                    ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
                ],
            ]
        );

        $inputFilter->add(
            [
            'name'       => 'description',
            'required'   => true,
            'filters'    => [['name' => 'StringTrim']],
            'validators' => [
                ['name' => 'NotEmpty'],
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 50000]],
            ],
            ]
        );

        $inputFilter->add(
            [
            'name'     => 'is_active',
            'required' => false,
            'filters'  => [['name' => 'Boolean']],
            ]
        );

        return $inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception('Not used');
    }
}
