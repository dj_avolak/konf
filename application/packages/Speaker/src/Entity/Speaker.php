<?php

declare(strict_types=1);

namespace Speaker\Entity;

/**
 * Class speaker.
 */
class Speaker
{
    private $speaker_uuid;
    private $speaker_id;
    private $name;
    private $description;
    private $main_img;
    private $is_active;
    private $created_at;
    private $slug;
    private $company;
    private $twitter;
    private $facebook;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * speaker constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return binary
     */
    public function getSpeakerUuid()
    {
        return $this->speaker_uuid;
    }

    /**
     * @param $speaker_uuid
     */
    public function setSpeakerUuid($speaker_uuid)
    {
        $this->speaker_uuid = $speaker_uuid;
    }

    /**
     * @return mixed
     */
    public function getSpeakerId()
    {
        return $this->speaker_id;
    }

    /**
     * @param $speaker_id
     */
    public function setSpeakerId($speaker_id)
    {
        $this->speaker_id = $speaker_id;
    }

    /**
     * @param null|int $limit break string to smaller one
     *
     * @return string
     */
    public function getDescription($limit = null)
    {
        if (!$limit) {
            return $this->description;
        }

        return mb_substr($this->description, 0, $limit);
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getMainImg()
    {
        return $this->main_img;
    }

    /**
     * @param mixed $main_img
     */
    public function setMainImg($main_img)
    {
        $this->main_img = $main_img;
    }

    /**
     * @param null|string $format Format date eg. "H:i d.m.Y"
     *
     * @return false|string
     */
    public function getCreatedAt($format = null)
    {
        if (!$format) {
            return $this->created_at;
        }

        return date($format, strtotime($this->created_at));
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt(string $created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * Hydrating result set from database.
     *
     * @param array $data
     */
    public function exchangeArray($data = [])
    {
        foreach (array_keys(get_object_vars($this)) as $property) {
            $this->{$property} = isset($data[$property]) ? $data[$property]
                : null;
        }
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'speaker_uuid'         => (binary) $this->speaker_uuid,
            'speaker_id'           => (string) $this->speaker_id,
            'description'       => (string) $this->description,
            'main_img'          => (string) $this->main_img,
            'is_active'         => (bool) $this->is_active,
            'created_at'        => (string) $this->created_at,
            'slug'              => (string) $this->slug,
            'twitter'              => (string) $this->twitter,
            'facebook'              => (string) $this->facebook,
            'company'              => (string) $this->company,
        ];
    }
}
