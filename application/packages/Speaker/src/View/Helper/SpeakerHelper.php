<?php

declare(strict_types=1);

namespace Speaker\View\Helper;

use Speaker\Service\SpeakerService;
use Zend\View\Helper\AbstractHelper;

class SpeakerHelper extends AbstractHelper
{
    private $speakerService;

    public function __construct(SpeakerService $speakerService)
    {
        $this->speakerService = $speakerService;
    }

    public function __invoke()
    {
        return $this;
    }

    /**
     * Fetch all pages for select box.
     */
    public function forSelect()
    {
        return $this->speakerService->getForSelect();
    }
}
