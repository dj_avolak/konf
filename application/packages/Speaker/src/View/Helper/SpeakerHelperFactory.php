<?php

declare(strict_types=1);

namespace Speaker\View\Helper;

use Interop\Container\ContainerInterface;
use Speaker\Service\SpeakerService;

class SpeakerHelperFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new SpeakerHelper(
            $container->get(SpeakerService::class)
        );
    }
}
