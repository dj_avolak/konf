<?php

declare(strict_types=1);

namespace Speaker;

use Zend\ServiceManager\Factory\InvokableFactory;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'templates' => [
                'map'   => [
                    'speaker/pagination' => __DIR__.'/../templates/partial/pagination.php',
                ],
                'paths' => [
                    'speaker' => [__DIR__.'/../templates/speaker'],
                ],
            ],

            'dependencies' => [
                'factories' => [
                    Controller\SpeakerController::class => Controller\SpeakerControllerFactory::class,
                    Service\SpeakerService::class       => Service\SpeakerServiceFactory::class,
                    Mapper\SpeakerMapper::class         => Mapper\SpeakerMapperFactory::class,
                    Filter\SpeakerFilter::class         => InvokableFactory::class,
                ],
            ],

            'routes' => [
                [
                    'name'            => 'admin.speakers',
                    'path'            => '/admin/speakers',
                    'middleware'      => Controller\SpeakerController::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'            => 'admin.speakers.action',
                    'path'            => '/admin/speakers/{action}/{id}',
                    'middleware'      => Controller\SpeakerController::class,
                    'allowed_methods' => ['GET', 'POST'],
                ],
            ],

            'view_helpers' => [
                'factories' => [
                    'speaker'  => View\Helper\SpeakerHelperFactory::class,
                ],
            ],
        ];
    }
}
