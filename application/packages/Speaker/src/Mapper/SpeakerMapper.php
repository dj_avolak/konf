<?php

declare(strict_types=1);

namespace Speaker\Mapper;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class SpeakerMapper extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'speaker';

    public function setDbAdapter(Adapter $adapter)
    {
        throw new \Exception('Set DB adapter in constructor.', 400);
    }

    public function __construct(Adapter $adapter, HydratingResultSet $resultSet)
    {
        $this->resultSetPrototype = $resultSet;
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function getPaginationSelect()
    {
        return $this->getSql()->select()->order(
            [
            'created_at'  => 'desc',
            ]
        );
    }

    public function getRandomActiveSpeakers()
    {
        $sql = new Sql( $this->adapter ) ;
        $select = $this->getSql()->select()->where('is_active = 1')->order(new Expression('RAND()'))->limit(5);

        return $sql->prepareStatementForSqlObject($select)->execute();
    }

    public function getActiveSpeakers()
    {
        $sql = new Sql( $this->adapter ) ;
        $select = $this->getSql()->select()->where('is_active = 1')->order([
            'name'  => 'asc',
        ]);

        return $sql->prepareStatementForSqlObject($select)->execute();
    }
}
