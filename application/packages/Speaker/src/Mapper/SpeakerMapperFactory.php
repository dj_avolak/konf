<?php

declare(strict_types=1);

namespace Speaker\Mapper;

use Interop\Container\ContainerInterface;
use Speaker\Entity\Speaker;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\ArraySerializable;

class SpeakerMapperFactory
{
    public function __invoke(ContainerInterface $container): SpeakerMapper
    {
        $adapter = $container->get(Adapter::class);
        $resultSet = new HydratingResultSet(new ArraySerializable(), new Speaker());

        return new SpeakerMapper($adapter, $resultSet);
    }
}
