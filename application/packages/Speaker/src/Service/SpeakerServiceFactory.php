<?php

declare(strict_types=1);

namespace Speaker\Service;

use Interop\Container\ContainerInterface;
use Speaker\Filter\SpeakerFilter;
use Speaker\Mapper\SpeakerMapper;
use UploadHelper\Upload;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class SpeakerServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['upload'];
        $upload = new Upload($config['public_path'], $config['non_public_path']);

        // Create pagination object
        $talkMapper = $container->get(SpeakerMapper::class);
        $select = $talkMapper->getPaginationSelect();
        $paginationAdapter = new DbSelect($select, $talkMapper->getAdapter(), $talkMapper->getResultSetPrototype());
        $pagination = new Paginator($paginationAdapter);

        return new SpeakerService(
            $container->get(SpeakerFilter::class),
            $talkMapper,
            $pagination,
            $upload
        );
    }
}
