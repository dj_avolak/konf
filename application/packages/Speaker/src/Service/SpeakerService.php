<?php

declare(strict_types=1);

namespace Speaker\Service;

use MysqlUuid\Formats\Binary;
use MysqlUuid\Uuid as MysqlUuid;
use Speaker\Filter\SpeakerFilter;
use Speaker\Mapper\SpeakerMapper;
use Ramsey\Uuid\Uuid;
use Std\FilterException;
use UploadHelper\Upload;
use Zend\Paginator\Paginator;

class SpeakerService
{
    private $speakerFilter;
    private $speakerMapper;
    private $pagination;
    private $upload;

    public function __construct(SpeakerFilter $speakerFilter, SpeakerMapper $speakerMapper, Paginator $pagination, Upload $upload)
    {
        $this->speakerFilter = $speakerFilter;
        $this->speakerMapper = $speakerMapper;
        $this->pagination = $pagination;
        $this->upload = $upload;
    }

    public function getActiveSpeakers()
    {
        return $this->speakerMapper->getActiveSpeakers();
    }

    public function getHomepageSpeakers()
    {
        return $this->speakerMapper->getRandomActiveSpeakers();
    }

    public function getPagination($page = 1, $limit = 10)
    {
        $this->pagination->setCurrentPageNumber($page);
        $this->pagination->setItemCountPerPage($limit);

        return $this->pagination;
    }

    /**
     * @param $speakerId
     *
     * @return \Speaker\Entity\Speaker|null
     */
    public function getSpeaker($speakerId)
    {
        return $this->speakerMapper->select(['Speaker_id' => $speakerId])->current();
    }

    /**
     * @param array $data
     *
     * @throws FilterException
     *
     * @return int
     */
    public function createSpeaker($data)
    {
        $filter = $this->speakerFilter->getInputFilter()->setData($data);

        if (!$filter->isValid()) {
            throw new FilterException($filter->getMessages());
        }

        $data = $filter->getValues()
            + ['main_img' => $this->upload->uploadImage($data, 'main_img')];
        $data['speaker_id'] = Uuid::uuid1()->toString();
        $data['speaker_uuid']
            = (new MysqlUuid($data['speaker_id']))->toFormat(new Binary());

        return $this->speakerMapper->insert($data);
    }

    public function updateSpeaker($data, $speakerId)
    {
        if (!($speaker = $this->getSpeaker($speakerId))) {
            throw new \Exception('Speaker object not found. Speaker ID:'.$speakerId);
        }

        $filter = $this->speakerFilter->getInputFilter()->setData($data);

        if (!$filter->isValid()) {
            throw new FilterException($filter->getMessages());
        }

        $data = $filter->getValues()
            + ['main_img' => $this->upload->uploadImage($data, 'main_img')];

        // We don't want to force user to re-upload image on edit
        if (!$data['main_img']) {
            unset($data['main_img']);
        } else {
            $this->upload->deleteFile($speaker->getMainImg());
        }

        return $this->speakerMapper->update($data, ['Speaker_id' => $speakerId]);
    }

    public function delete($speakerId)
    {
        if (!($speaker = $this->getSpeaker($speakerId))) {
            throw new \Exception('Speaker not found');
        }

        $this->upload->deleteFile($speaker->getMainImg());

        return (bool) $this->speakerMapper->delete(['Speaker_id' => $speakerId]);
    }

    public function getForSelect($where)
    {
        return $this->speakerMapper->select($where);
    }
}
