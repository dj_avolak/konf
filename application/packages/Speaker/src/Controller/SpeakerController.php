<?php

namespace Speaker\Controller;

use Speaker\Service\SpeakerService;
use Std\AbstractController;
use Std\FilterException;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface as Router;
use Zend\Expressive\Template\TemplateRendererInterface as Template;
use Zend\Http\PhpEnvironment\Request;

class SpeakerController extends AbstractController
{
    /**
     * @var Template
     */
    private $template;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var SpeakerService
     */
    private $speakerService;

    /**
     * speakerController constructor.
     *
     * @param Template    $template
     * @param Router      $router
     * @param SpeakerService $speakerService
     */
    public function __construct(Template $template, Router $router, SpeakerService $speakerService)
    {
        $this->template = $template;
        $this->router = $router;
        $this->speakerService = $speakerService;
    }

    /**
     * @return HtmlResponse
     */
    public function index(): HtmlResponse
    {
        $params = $this->request->getQueryParams();
        $page = isset($params['page']) ? $params['page'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 15;
        $pagination = $this->speakerService->getPagination($page, $limit);

        return new HtmlResponse(
            $this->template->render(
                'speaker::index', [
                    'pagination' => $pagination,
                    'layout'     => 'layout/admin',
                ]
            )
        );
    }

    public function edit($errors = []): HtmlResponse
    {
        $id = $this->request->getAttribute('id');
        $speaker = $this->speakerService->getSpeaker($id);

        if ($this->request->getParsedBody()) {
            $speaker = new \Speaker\Entity\Speaker();
            $speaker->exchangeArray($this->request->getParsedBody() + (array) $speaker);
            $speaker->setSpeakerId($id);
        }

        return new HtmlResponse(
            $this->template->render(
                'speaker::edit', [
                    'speaker'   => $speaker,
                    'errors' => $errors,
                    'layout' => 'layout/admin',
                ]
            )
        );
    }

    public function save(): \Psr\Http\Message\ResponseInterface
    {
        try {
            $speakerId = $this->request->getAttribute('id');
            $data = $this->request->getParsedBody();
            $data += (new Request())->getFiles()->toArray();

            if ($speakerId) {
                $this->speakerService->updateSpeaker($data, $speakerId);
            } else {
                $this->speakerService->createSpeaker($data);
            }

            return $this->response->withStatus(302)->withHeader('Location', $this->router->generateUri('admin.speakers'));
        } catch (FilterException $fe) {
            return $this->edit($fe->getArrayMessages());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete(): \Psr\Http\Message\ResponseInterface
    {
        try {
            $speakerId = $this->request->getAttribute('id');
            $this->speakerService->delete($speakerId);

            return $this->response->withStatus(302)->withHeader('Location', $this->router->generateUri('admin.speakers'));
        } catch (\Exception $e) {
            return $this->response->withStatus(302)->withHeader('Location', $this->router->generateUri('admin.speakers'));
        }
    }
}
