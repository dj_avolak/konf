<?php

declare(strict_types=1);

namespace Speaker\Controller;

use Interop\Container\ContainerInterface;
use Speaker\Service\SpeakerService;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class SpeakerControllerFactory
{
    public function __invoke(ContainerInterface $container): SpeakerController
    {
        return new SpeakerController(
            $container->get(TemplateRendererInterface::class),
            $container->get(RouterInterface::class),
            $container->get(SpeakerService::class)
        );
    }
}
