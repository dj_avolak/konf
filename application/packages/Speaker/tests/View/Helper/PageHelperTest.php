<?php

declare(strict_types=1);

namespace Page\Test\View\Helper;

class PageHelperTest extends \PHPUnit_Framework_TestCase
{
    public function testInvokingPageHelperShouldReturnItSelf()
    {
        $pageService = $this->getMockBuilder(\Page\Service\TalkService::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $pageHelper = new \Page\View\Helper\TalkHelper($pageService);
        static::assertInstanceOf(\Page\View\Helper\TalkHelper::class, $pageHelper());
    }

    public function testForSelectShouldReturnArray()
    {
        $pageService = $this->getMockBuilder(\Page\Service\TalkService::class)
            ->setMethods(['getForSelect'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $pageService->expects(static::once())
            ->method('getForSelect')
            ->willReturn([]);
        $pageHelper = new \Page\View\Helper\TalkHelper($pageService);
        static::assertSame([], $pageHelper->forSelect());
    }
}
