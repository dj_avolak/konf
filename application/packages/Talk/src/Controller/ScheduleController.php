<?php

namespace Talk\Controller;

use Talk\Service\TalkService;
use Speaker\Service\SpeakerService;
use Std\AbstractController;
use Std\FilterException;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Router\RouterInterface as Router;
use Zend\Expressive\Template\TemplateRendererInterface as Template;
use Zend\Http\PhpEnvironment\Request;

class ScheduleController extends AbstractController
{
    /**
     * @var Template
     */
    private $template;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var TalkService
     */
    private $talkService;

    private $speakerService;

    /**
     * talkController constructor.
     *
     * @param Template    $template
     * @param Router      $router
     * @param TalkService $talkService
     */
    public function __construct(Template $template, Router $router, TalkService $talkService, SpeakerService $speakerService)
    {
        $this->template = $template;
        $this->router = $router;
        $this->talkService = $talkService;
        $this->speakerService = $speakerService;
    }

    /**
     * @return HtmlResponse
     */
    public function index(): HtmlResponse
    {
        $params = $this->request->getQueryParams();
        $schedule = isset($params['schedule']) ? $params['schedule'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 15;
        $pagination = $this->talkService->getPagination($schedule, $limit);

        return new HtmlResponse(
            $this->template->render(
                'schedule::index', [
                    'pagination' => $pagination,
                    'layout'     => 'layout/admin',
                ]
            )
        );
    }

    public function edit($errors = []): HtmlResponse
    {
        $id = $this->request->getAttribute('id');
        $talk = $this->talkService->getTalk($id);

        if ($this->request->getParsedBody()) {
            $talk = new \Talk\Entity\Talk();
            $talk->exchangeArray($this->request->getParsedBody() + (array) $talk);
            $talk->settalkId($id);
        }

        return new HtmlResponse(
            $this->template->render(
                'talk::edit', [
                    'talk'   => $talk,
                    'errors' => $errors,
                    'speakers' => $this->speakerService->getPagination(1, 100),
                    'layout' => 'layout/admin',
                ]
            )
        );
    }

    public function save(): \Psr\Http\Message\ResponseInterface
    {
        try {
            $talkId = $this->request->getAttribute('id');
            $data = $this->request->getParsedBody();
            $data += (new Request())->getFiles()->toArray();

            if ($talkId) {
                $this->talkService->updatetalk($data, $talkId);
            } else {
                $this->talkService->createtalk($data);
            }

            return $this->response->withStatus(302)->withHeader('Location', $this->router->generateUri('admin.talks'));
        } catch (FilterException $fe) {
            return $this->edit($fe->getArrayMessages());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete(): \Psr\Http\Message\ResponseInterface
    {
        try {
            $talkId = $this->request->getAttribute('id');
            $this->talkService->delete($talkId);

            return $this->response->withStatus(302)->withHeader('Location', $this->router->generateUri('admin.talks'));
        } catch (\Exception $e) {
            return $this->response->withStatus(302)->withHeader('Location', $this->router->generateUri('admin.talks'));
        }
    }
}
