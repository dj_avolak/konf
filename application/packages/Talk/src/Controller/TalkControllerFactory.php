<?php

declare(strict_types=1);

namespace Talk\Controller;

use Interop\Container\ContainerInterface;
use Talk\Service\TalkService;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Speaker\Service\SpeakerService;

class TalkControllerFactory
{
    public function __invoke(ContainerInterface $container): TalkController
    {
        return new TalkController(
            $container->get(TemplateRendererInterface::class),
            $container->get(RouterInterface::class),
            $container->get(TalkService::class),
            $container->get(SpeakerService::class)
        );
    }
}
