<?php

declare(strict_types=1);

namespace Talk;

use Zend\ServiceManager\Factory\InvokableFactory;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'templates' => [
                'map'   => [
                    'talk/pagination' => __DIR__.'/../templates/partial/pagination.php',
                ],
                'paths' => [
                    'talk' => [__DIR__.'/../templates/talk'],
                    'schedule' => [__DIR__.'/../templates/schedule'],
                ],
            ],

            'dependencies' => [
                'factories' => [
                    Controller\TalkController::class => Controller\TalkControllerFactory::class,
                    Controller\ScheduleController::class => Controller\ScheduleControllerFactory::class,
                    Service\TalkService::class       => Service\TalkServiceFactory::class,
                    Mapper\TalkMapper::class         => Mapper\TalkMapperFactory::class,
//                    Mapper\TalkMapper::class         => \Std\MapperFactory::class,
                    Filter\TalkFilter::class         => InvokableFactory::class,
                ],
            ],

            'routes' => [
                [
                    'name'            => 'admin.talks',
                    'path'            => '/admin/talks',
                    'middleware'      => Controller\TalkController::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'            => 'admin.talks.action',
                    'path'            => '/admin/talks/{action}/{id}',
                    'middleware'      => Controller\TalkController::class,
                    'allowed_methods' => ['GET', 'POST'],
                ],
                [
                    'name'            => 'admin.schedule',
                    'path'            => '/admin/schedule',
                    'middleware'      => Controller\ScheduleController::class,
                    'allowed_methods' => ['GET', 'POST'],
                ],
                [
                    'name'            => 'admin.schedule.action',
                    'path'            => '/admin/schedule/{action}/{id}',
                    'middleware'      => Controller\ScheduleController::class,
                    'allowed_methods' => ['GET', 'POST'],
                ],
            ],

            'view_helpers' => [
                'factories' => [
                    'talk'  => View\Helper\TalkHelperFactory::class,
                ],
            ],
        ];
    }
}
