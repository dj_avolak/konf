<?php

declare(strict_types=1);

namespace Talk\View\Helper;

use Interop\Container\ContainerInterface;
use Talk\Service\TalkService;

class TalkHelperFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new TalkHelper(
            $container->get(TalkService::class)
        );
    }
}
