<?php

declare(strict_types=1);

namespace Talk\View\Helper;

use Talk\Service\TalkService;
use Zend\View\Helper\AbstractHelper;

class TalkHelper extends AbstractHelper
{
    private $talkService;

    public function __construct(TalkService $talkService)
    {
        $this->talkService = $talkService;
    }

    public function __invoke()
    {
        return $this;
    }

    /**
     * Fetch all pages for select box.
     */
    public function forSelect()
    {
        return $this->talkService->getForSelect();
    }
}
