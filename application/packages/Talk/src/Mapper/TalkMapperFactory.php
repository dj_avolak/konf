<?php

declare(strict_types=1);

namespace Talk\Mapper;

use Interop\Container\ContainerInterface;
use Talk\Entity\Talk;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\ArraySerializable;

class TalkMapperFactory
{
    public function __invoke(ContainerInterface $container): TalkMapper
    {
        $adapter = $container->get(Adapter::class);
        $resultSet = new HydratingResultSet(new ArraySerializable(), new Talk());

        return new TalkMapper($adapter, $resultSet);
    }
}
