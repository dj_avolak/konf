<?php

declare(strict_types=1);

namespace Talk\Mapper;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Sql;
use Talk\Entity\Talk;

class TalkMapper extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'talk';

    public function setDbAdapter(Adapter $adapter)
    {
        throw new \Exception('Set DB adapter in constructor.', 400);
    }

    public function __construct(Adapter $adapter, HydratingResultSet $resultSet)
    {
        $this->resultSetPrototype = $resultSet;
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function getActiveTalks()
    {
        $sql = new Sql( $this->adapter ) ;
        $select = $sql->select() ;
        $select -> from ( $this->table )
            ->join(['s' => 'speaker'], 'talk.speaker_id = s.speaker_id', ['speaker_id', 'speaker' => 'name', 'speaker_slug' => 'slug'])
            ->where('talk.is_active = 1')->order('talk.type DESC, talk.title ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    public function getActiveWorkshops()
    {
        $sql = new Sql( $this->adapter ) ;
        $select = $sql->select() ;
        $select -> from ( $this->table )
            ->join(['s' => 'speaker'], 'talk.speaker_id = s.speaker_id', ['speaker_id', 'speaker' => 'name', 'speaker_slug' => 'slug'])
            ->where('talk.is_active = 1 AND talk.type = ' . Talk::TYPE_WORKSHOP)->order('talk.type DESC, talk.title ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    public function getPaginationSelect()
    {
        return $this->getSql()->select()->order(
            [
            'created_at'  => 'desc',
            ]
        );
    }

}
