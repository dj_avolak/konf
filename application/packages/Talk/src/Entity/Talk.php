<?php

declare(strict_types=1);

namespace Talk\Entity;

/**
 * Class talk.
 */
class Talk
{
    const TRACK_MAJESTIC = 1;
    const TRACK_PRACTICUM = 2;
    const TRACK_WORKSHOP = 3;

    const LEVEL_ENTRY = 1;
    const LEVEL_MID = 2;
    const LEVEL_ADVANCED = 3;

    const TYPE_STANDARD = 1;
    const TYPE_KEYNOTE = 2;
    const TYPE_WORKSHOP = 3;

    private $talk_uuid;
    private $talk_id;
    private $title;
    private $description;
    private $main_img;
    private $is_active;
    private $created_at;
    private $slug;
    private $track;
    private $speaker_id;
    private $is_wysiwyg_editor;
    private $scheduled_at;
    private $type;
    private $level;

    public function getHrType()
    {
        return [
            self::TYPE_STANDARD => 'Talk',
            self::TYPE_KEYNOTE => 'Keynote',
            self::TYPE_WORKSHOP => 'Workshop',
        ];
    }

    public function getHrLevel()
    {
        return [
            self::LEVEL_ENTRY => 'Entry level',
            self::LEVEL_MID => 'Mid level',
            self::LEVEL_ADVANCED => 'Advanced level'
        ];
    }

    public function getHrTrack()
    {
        return [
            self::TRACK_MAJESTIC => 'Majestic',
            self::TRACK_PRACTICUM => 'Practicum',
            self::TRACK_WORKSHOP => 'Workshop',
        ];
    }

    public function getScheduledAt($format = null)
    {
        if (!$format || !$this->scheduled_at) {
            return $this->scheduled_at;
        }

        return date($format, strtotime($this->scheduled_at));
    }

    public function setScheduledAt($time)
    {
        $this->scheduled_at = $time;
    }

    /**
     * @return mixed
     */
    public function getTrack()
    {
        return (int) $this->track;
    }

    /**
     * @param mixed $track
     */
    public function setTrack($track)
    {
        $this->track = $track;
    }

    /**
     * @return mixed
     */
    public function getIsWysiwygEditor()
    {
        return $this->is_wysiwyg_editor;
    }

    /**
     * @param mixed $is_wysiwyg_editor
     */
    public function setIsWysiwygEditor($is_wysiwyg_editor)
    {
        $this->is_wysiwyg_editor = $is_wysiwyg_editor;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * talk constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return binary
     */
    public function getTalkUuid()
    {
        return $this->talk_uuid;
    }

    /**
     * @param $talk_uuid
     */
    public function setTalkUuid($talk_uuid)
    {
        $this->talk_uuid = $talk_uuid;
    }

    /**
     * @return mixed
     */
    public function getTalkId()
    {
        return $this->talk_id;
    }

    /**
     * @param $talk_id
     */
    public function setTalkId($talk_id)
    {
        $this->talk_id = $talk_id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param null|int $limit break string to smaller one
     *
     * @return string
     */
    public function getDescription($limit = null)
    {
        if (!$limit) {
            return $this->description;
        }

        return mb_substr($this->description, 0, $limit);
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getMainImg()
    {
        return $this->main_img;
    }

    /**
     * @param mixed $main_img
     */
    public function setMainImg($main_img)
    {
        $this->main_img = $main_img;
    }

    /**
     * @param null|string $format Format date eg. "H:i d.m.Y"
     *
     * @return false|string
     */
    public function getCreatedAt($format = null)
    {
        if (!$format) {
            return $this->created_at;
        }

        return date($format, strtotime($this->created_at));
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt(string $created_at)
    {
        $this->created_at = $created_at;
    }

    public function getSpeakerId()
    {
        return $this->speaker_id;
    }

    public function setSpeakerId($speakerId)
    {
        $this->speaker_id = $speakerId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return (int) $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return (int) $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * Hydrating result set from database.
     *
     * @param array $data
     */
    public function exchangeArray($data = [])
    {
        foreach (array_keys(get_object_vars($this)) as $property) {
            $this->{$property} = isset($data[$property]) ? $data[$property]
                : null;
        }
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return [
            'talk_uuid'         => (binary) $this->talk_uuid,
            'talk_id'           => (string) $this->talk_id,
            'speaker_id'        => (string) $this->speaker_id,
            'title'             => (string) $this->title,
            'description'       => (string) $this->description,
            'main_img'          => (string) $this->main_img,
            'is_active'         => (bool) $this->is_active,
            'is_wysiwyg_editor' => (bool) $this->is_wysiwyg_editor,
            'created_at'        => (string) $this->created_at,
            'slug'              => (string) $this->slug,
            'track'              => (string) $this->track,
            'scheduled_at'        => (string) $this->scheduled_at,
            'level'        => $this->level,
            'type'        => $this->type,
        ];
    }
}
