<?php

declare(strict_types=1);

namespace Talk\Service;

use Interop\Container\ContainerInterface;
use Talk\Filter\TalkFilter;
use Talk\Mapper\TalkMapper;
use UploadHelper\Upload;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class TalkServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['upload'];
        $upload = new Upload($config['public_path'], $config['non_public_path']);

        // Create pagination object
        $talkMapper = $container->get(TalkMapper::class);
        $select = $talkMapper->getPaginationSelect();
        $paginationAdapter = new DbSelect($select, $talkMapper->getAdapter(), $talkMapper->getResultSetPrototype());
        $pagination = new Paginator($paginationAdapter);

        return new TalkService(
            $container->get(TalkFilter::class),
            $talkMapper,
            $pagination,
            $upload
        );
    }
}
