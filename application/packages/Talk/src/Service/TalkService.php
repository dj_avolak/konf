<?php

declare(strict_types=1);

namespace Talk\Service;

use MysqlUuid\Formats\Binary;
use MysqlUuid\Uuid as MysqlUuid;
use Talk\Filter\TalkFilter;
use Talk\Mapper\TalkMapper;
use Ramsey\Uuid\Uuid;
use Std\FilterException;
use UploadHelper\Upload;
use Zend\Paginator\Paginator;

class TalkService
{
    private $talkFilter;
    private $talkMapper;
    private $pagination;
    private $upload;

    public function __construct(TalkFilter $talkFilter, TalkMapper $talkMapper, Paginator $pagination, Upload $upload)
    {
        $this->talkFilter = $talkFilter;
        $this->talkMapper = $talkMapper;
        $this->pagination = $pagination;
        $this->upload = $upload;
    }

    public function getPagination($page = 1, $limit = 10)
    {
        $this->pagination->setCurrentPageNumber($page);
        $this->pagination->setItemCountPerPage($limit);

        return $this->pagination;
    }

    /**
     * @param $talkId
     *
     * @return \Talk\Entity\Talk|null
     */
    public function getTalk($talkId)
    {
        return $this->talkMapper->select(['talk_id' => $talkId])->current();
    }

    public function getForWeb()
    {
        return $this->talkMapper->getActiveTalks();
    }

    public function getActiveWorkshops()
    {
        return $this->talkMapper->getActiveWorkshops();
    }

    public function getSchedule()
    {
        return $this->getForSelect('is_active = 1');
    }

    /**
     * @param array $data
     *
     * @throws FilterException
     *
     * @return int
     */
    public function createTalk($data)
    {
        $filter = $this->talkFilter->getInputFilter()->setData($data);

        if (!$filter->isValid()) {
            throw new FilterException($filter->getMessages());
        }

        $data = $filter->getValues()
            + ['main_img' => $this->upload->uploadImage($data, 'main_img')];
        $data['talk_id'] = Uuid::uuid1()->toString();
        $data['talk_uuid']
            = (new MysqlUuid($data['talk_id']))->toFormat(new Binary());

        return $this->talkMapper->insert($data);
    }

    public function updateTalk($data, $talkId)
    {
        if (!($talk = $this->getTalk($talkId))) {
            throw new \Exception('Talk object not found. Talk ID:'.$talkId);
        }

        $filter = $this->talkFilter->getInputFilter()->setData($data);

        if (!$filter->isValid()) {
            throw new FilterException($filter->getMessages());
        }

        $data = $filter->getValues()
            + ['main_img' => $this->upload->uploadImage($data, 'main_img')];

        // We don't want to force user to re-upload image on edit
        if (!$data['main_img']) {
            unset($data['main_img']);
        } else {
            $this->upload->deleteFile($talk->getMainImg());
        }

        return $this->talkMapper->update($data, ['talk_id' => $talkId]);
    }

    public function delete($talkId)
    {
        if (!($talk = $this->getTalk($talkId))) {
            throw new \Exception('Talk not found');
        }

        $this->upload->deleteFile($talk->getMainImg());

        return (bool) $this->talkMapper->delete(['talk_id' => $talkId]);
    }

    public function getForSelect($where)
    {
        return $this->talkMapper->select($where);
    }
}
