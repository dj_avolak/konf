<?php

declare(strict_types=1);

namespace Web\Action;

use Speaker\Service\SpeakerService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;

/**
 * Class SpeakerAction.
 */
class SpeakerAction
{
    /** @var Template */
    private $template;

    /** @var SpeakerService $speakerService */
    private $speakerService;

    /**
     * PageAction constructor.
     *
     * @param Template       $template
     * @param SpeakerService $speakerService
     */
    public function __construct(Template $template, SpeakerService $speakerService)
    {
        $this->template = $template;
        $this->speakerService = $speakerService;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return HtmlResponse
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
//        $urlSlug = $request->getAttribute('url_slug');
        $speakers = $this->speakerService->getActiveSpeakers();

        return new HtmlResponse($this->template->render('web::speaker', [
            'speakers'   => $speakers,
            'layout'   => 'layout/web',
        ]));
    }
}
