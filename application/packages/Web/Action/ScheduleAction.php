<?php

declare(strict_types=1);

namespace Web\Action;

use Talk\Service\TalkService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;

/**
 * Class ScheduleAction.
 */
class ScheduleAction
{
    /** @var Template */
    private $template;

    /** @var TalkService $talkService */
    private $talkService;

    /**
     * PageAction constructor.
     *
     * @param Template    $template
     * @param TalkService $talkService
     */
    public function __construct(Template $template, TalkService $talkService)
    {
        $this->template = $template;
        $this->talkService = $talkService;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return HtmlResponse
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
        $talks = $this->talkService->getSchedule();

        return new HtmlResponse($this->template->render('web::schedule', [
            'talks'   => $talks,
            'layout'   => 'layout/web',
        ]));
    }
}
