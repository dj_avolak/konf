<?php

declare(strict_types=1);

namespace Web\Action;

use Article\Entity\ArticleType;
use Article\Service\PostService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;
use Category\Service\CategoryService;

/**
 * Class PostAction.
 */
class PostAction
{
    /** @var Template */
    private $template;

    /** @var PostService */
    private $postService;

    /** @var CategoryService */
    private $categoryService;

    /**
     * PostAction constructor.
     *
     * @param Template $template
     */
    public function __construct(Template $template, PostService $postService, CategoryService $categoryService)
    {
        $this->template = $template;
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return HtmlResponse
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
        $post = $this->postService->fetchSingleArticleBySlug($request->getAttribute('article_id'));
        if (!$post || $post->type != ArticleType::POST) {
            return $next($request, $response);
        }
        $category = $this->categoryService->getCategoryBySlug('blog');
        $posts = $this->categoryService->paginateCategoryPosts($category, 1);

        return new HtmlResponse($this->template->render('web::post', [
            'layout'   => 'layout/web',
            'post'     => $post,
            'posts'    => $posts,
        ]));
    }
}
