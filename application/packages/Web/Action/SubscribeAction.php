<?php

declare(strict_types=1);

namespace Web\Action;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;
use GuzzleHttp\Client;

/**
 * Class SubscribeAction.
 */
class SubscribeAction
{
    /** @var Template */
    private $template;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $listId;

    /**
     * SubscribeAction constructor.
     *
     * @param Template $template
     * @param Client $client
     * @param $listId
     */
    public function __construct(Template $template, Client $client, $listId)
    {
        $this->template = $template;
        $this->httpClient = $client;
        $this->listId = $listId;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
        $data = [
            'email_address' => $request->getParsedBody()['email'],
            'status'        => 'subscribed',
        ];
        $responseData['success'] = true;
        $uri = "lists/{$this->listId}/members/";
        $request = new \GuzzleHttp\Psr7\Request('POST', $uri, [], \GuzzleHttp\json_encode($data));
        try {
            $response = $this->httpClient->send($request);
        } catch (\Exception $e) {
            $data['success'] = false;
//            var_dump($e->getMessage());
//            var_dump(\GuzzleHttp\json_decode($response->getBody()->getContents()));
//            die();
        }

        return new JsonResponse($data);
    }
}
