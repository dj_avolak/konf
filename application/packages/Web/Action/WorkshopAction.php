<?php

declare(strict_types=1);

namespace Web\Action;

use Talk\Service\TalkService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;
use Page\Service\PageService;

/**
 * Class WorkshopAction.
 */
class WorkshopAction
{
    /** @var Template */
    private $template;

    /** @var TalkService $talkService */
    private $talkService;

    /** @var PageService $pageService */
    private $pageService;

    /**
     * PageAction constructor.
     *
     * @param Template    $template
     * @param TalkService $talkService
     */
    public function __construct(Template $template, TalkService $talkService, PageService $pageService)
    {
        $this->template = $template;
        $this->talkService = $talkService;
        $this->pageService = $pageService;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return HtmlResponse
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
        $workshops = $this->talkService->getActiveWorkshops();
        $page = $this->pageService->getPageBySlug('workshops');

        return new HtmlResponse($this->template->render('web::workshops', [
            'talks'   => $workshops,
            'page'   => $page,
            'layout'   => 'layout/web',
        ]));
    }
}
