<?php

declare(strict_types=1);

namespace Web\Action;

use Article\Entity\ArticleType;
use Article\Service\PostService;
use Category\Service\CategoryService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;

/**
 * Class PostsAction.
 */
class PostsAction
{
    /** @var Template */
    private $template;

    /** @var PostService */
    private $postService;

    /** @var CategoryService */
    private $categoryService;

    /**
     * CategoryAction constructor.
     *
     * @param Template        $template
     * @param PostService     $postService
     * @param CategoryService $categoryService
     */
    public function __construct(
        Template $template,
        PostService $postService,
        CategoryService $categoryService
    ) {
        $this->template = $template;
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return HtmlResponse
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
        $params = $request->getQueryParams();
        $page = isset($params['page']) ? $params['page'] : 1;
        $category = $this->categoryService->getCategoryBySlug('blog');
        $posts = $this->categoryService->paginateCategoryPosts($category, $page);

        return new HtmlResponse($this->template->render('web::posts', [
            'layout'          => 'layout/web',
            'currentCategory' => $category,
            'posts'           => $posts,
        ]));
    }
}
