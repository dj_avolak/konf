<?php

declare(strict_types=1);

namespace Web\Action;

use Page\Service\PageService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Template\TemplateRendererInterface as Template;

/**
 * Class PageAction.
 */
class ContactAction
{
    /** @var Template */
    private $template;

    /** @var PageService $pageService */
    private $pageService;

    /**
     * PageAction constructor.
     *
     * @param Template    $template
     * @param PageService $pageService
     */
    public function __construct(Template $template, PageService $pageService)
    {
        $this->template = $template;
        $this->pageService = $pageService;
    }

    /**
     * Executed when action is invoked.
     *
     * @param Request       $request
     * @param Response      $response
     * @param callable|null $next
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function __invoke(
        Request $request,
        Response $response,
        callable $next = null
    ) {
        if ($request->getUri()->getPath() == '/contact-submit') {
            $to = 'conference@phpsrbija.rs';
//            $to = 'djavolak@mail.ru';
            $subject = "Conference app :: New contact form message received";
            $from = $request->getParsedBody()['emailContactForm'];
            $fromName = $request->getParsedBody()['nameContactForm'];
            $body = "New message received from:" . PHP_EOL;
            $body .= $fromName . "<{$from}>" . PHP_EOL;
            $body .= $request->getParsedBody()['textareaContactForm'];

            $data = [
                'to' => $to,
                'from' => 'Mailgun Sandbox <postmaster@sandboxa695b534aa764bd0bafa981e2672fa17.mailgun.org>',
//                'from' => $fromName . "<{$from}>",
                'text' => $body,
                'subject' => $subject,
            ];
            $domain = 'sandboxa695b534aa764bd0bafa981e2672fa17.mailgun.org';
            $apiKey = 'key-7e4e019fb467c597043be0688d6b884c';
            $mg = \Mailgun\Mailgun::create($apiKey);
            $response = $mg->messages()->send($domain, $data);

            $responseData['success'] = false;
            if ($response->getMessage() == 'Queued. Thank you.') {
                $responseData['success'] = true;
            }

            return new JsonResponse($data);
        }

        return new HtmlResponse($this->template->render('web::contact', [
            'layout' => 'layout/web',
//            'page'   => $page,
        ]));
    }
}
