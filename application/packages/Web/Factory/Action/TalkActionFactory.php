<?php

declare(strict_types=1);

namespace Web\Factory\Action;

use Interop\Container\ContainerInterface;
use Web\Action\TalkAction;
use Talk\Service\TalkService;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class TalkActionFactory.
 */
class TalkActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return TalkAction
     */
    public function __invoke(ContainerInterface $container): TalkAction
    {
        return new TalkAction(
            $container->get(TemplateRendererInterface::class),
            $container->get(TalkService::class)
        );
    }
}
