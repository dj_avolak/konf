<?php

declare(strict_types=1);

namespace Web\Factory\Action;

use Interop\Container\ContainerInterface;
use Page\Service\PageService;
use Web\Action\ContactAction;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class PageActionFactory.
 */
class ContactActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return ContactAction
     */
    public function __invoke(ContainerInterface $container): ContactAction
    {
        return new ContactAction(
            $container->get(TemplateRendererInterface::class),
            $container->get(PageService::class)
        );
    }
}
