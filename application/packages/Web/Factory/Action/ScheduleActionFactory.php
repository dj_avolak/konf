<?php

declare(strict_types=1);

namespace Web\Factory\Action;

use Interop\Container\ContainerInterface;
use Web\Action\ScheduleAction;
use Talk\Service\TalkService;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class ScheduleActionFactory.
 */
class ScheduleActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return ScheduleAction
     */
    public function __invoke(ContainerInterface $container): ScheduleAction
    {
        return new ScheduleAction(
            $container->get(TemplateRendererInterface::class),
            $container->get(TalkService::class)
        );
    }
}
