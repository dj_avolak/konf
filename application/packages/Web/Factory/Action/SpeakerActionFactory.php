<?php

declare(strict_types=1);

namespace Web\Factory\Action;

use Interop\Container\ContainerInterface;
use Speaker\Service\SpeakerService;
use Web\Action\SpeakerAction;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class SpeakerActionFactory.
 */
class SpeakerActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return SpeakerAction
     */
    public function __invoke(ContainerInterface $container): SpeakerAction
    {
        return new SpeakerAction(
            $container->get(TemplateRendererInterface::class),
            $container->get(SpeakerService::class)
        );
    }
}
