<?php

declare(strict_types=1);

namespace Web\Factory\Action;

use Interop\Container\ContainerInterface;
use Web\Action\WorkshopAction;
use Talk\Service\TalkService;
use Zend\Expressive\Template\TemplateRendererInterface;
use Page\Service\PageService;

/**
 * Class WorkshopActionFactory.
 */
class WorkshopActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return WorkshopAction
     */
    public function __invoke(ContainerInterface $container): WorkshopAction
    {
        return new WorkshopAction(
            $container->get(TemplateRendererInterface::class),
            $container->get(TalkService::class),
            $container->get(PageService::class)
        );
    }
}
