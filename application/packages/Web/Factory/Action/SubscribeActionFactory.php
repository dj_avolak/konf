<?php

declare(strict_types=1);

namespace Web\Factory\Action;

use Interop\Container\ContainerInterface;
use Web\Action\SubscribeAction;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class SubscribeActionFactory.
 */
class SubscribeActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return SubscribeAction
     */
    public function __invoke(ContainerInterface $container): SubscribeAction
    {

        $listId = $container->get('config')['mailchimp']['listId'];
        $endpoint = $container->get('config')['mailchimp']['endpoint'];
        $apiKey = $container->get('config')['mailchimp']['key'];
        $httpClient = new \GuzzleHttp\Client([
            'base_uri' => $endpoint,
            'auth' => ['demo', $apiKey, 'basic'],
        ]);

        return new SubscribeAction(
            $container->get(TemplateRendererInterface::class),
            $httpClient,
            $listId
        );
    }
}
