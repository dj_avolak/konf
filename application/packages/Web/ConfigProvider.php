<?php

namespace Web;

class ConfigProvider
{
    public function __invoke()
    {
        return [
            'templates' => [
                'map'   => [
                    'layout/web'         => 'templates/layout/web.phtml',
                    'layout/home'         => 'templates/layout/home.phtml',
                    'article/pagination' => 'templates/partial/pagination.phtml',
                ],
                'paths' => [
                    'templates' => ['templates'],
                    'web'       => ['templates/web'],
                ],
            ],

            'dependencies' => [
                'factories' => [
                    Action\HomeAction::class   => Factory\Action\HomeActionFactory::class,
                    Action\PageAction::class   => Factory\Action\PageActionFactory::class,
                    Action\PostsAction::class  => Factory\Action\PostsActionFactory::class,
                    Action\PostAction::class   => Factory\Action\PostActionFactory::class,
                    Action\ContactAction::class => Factory\Action\ContactActionFactory::class,
                    Action\VideoAction::class  => Factory\Action\VideoActionFactory::class,
                    Action\EventsAction::class => Factory\Action\EventsActionFactory::class,
                    Action\EventAction::class  => Factory\Action\EventActionFactory::class,
                    Action\SubscribeAction::class  => Factory\Action\SubscribeActionFactory::class,
                    Action\ScheduleAction::class  => Factory\Action\ScheduleActionFactory::class,
                    Action\SpeakerAction::class  => Factory\Action\SpeakerActionFactory::class,
                    Action\TalkAction::class  => Factory\Action\TalkActionFactory::class,
                    Action\WorkshopAction::class  => Factory\Action\WorkshopActionFactory::class,
                ],
            ],

            'routes' => [
                [
                    'name'       => 'home',
                    'path'       => '/',
                    'middleware' => Action\HomeAction::class,
                ],
                [
                    'name'       => 'workshops',
                    'path'       => '/workshops',
                    'middleware' => Action\WorkshopAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'       => 'speaker',
                    'path'       => '/speakers',
                    'middleware' => Action\SpeakerAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'       => 'sessions',
                    'path'       => '/sessions',
                    'middleware' => Action\TalkAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'       => 'schedule',
                    'path'       => '/schedule',
                    'middleware' => Action\ScheduleAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'       => 'subscribe',
                    'path'       => '/subscribe',
                    'middleware' => Action\SubscribeAction::class,
//                    'allowed_methods' => ['POST'],
                ],
                [
                    'name'       => 'contact-form',
                    'path'       => '/contact',
                    'middleware' => Action\ContactAction::class,
                    'allowed_methods' => ['GET'],
                ],
                [
                    'name'       => 'submit-contact-form',
                    'path'       => '/contact-submit',
                    'middleware' => Action\ContactAction::class,
                    'allowed_methods' => ['POST'],
                ],
                [
                    'name'       => 'page',
                    'path'       => '/{url_slug}',
                    'middleware' => Action\PageAction::class,
                ],
                [
                    'name'       => 'blog',
                    'path'       => '/blog/',
                    'middleware' => [
                        Action\PostsAction::class,
                    ],
                ],
                [
                    'name'       => 'post',
                    'path'       => '/post/{article_id}',
                    'middleware' => [
                        Action\PostAction::class,
                    ],
                ],
//                [
//                    'name'       => 'post',
//                    'path'       => '/{segment_1}/{segment_2}',
//                    'middleware' => [
//                        Action\PostAction::class,
//                        Action\VideoAction::class,
//                        Action\EventAction::class,
//                    ],
//                ],
            ],
        ];
    }
}
